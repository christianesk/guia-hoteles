$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();

    $('#carouselControls').carousel({
        interval: 1500
    });

    $('#btnEnviar').click(function () {
        $('#contact').modal('hide');
        $('#contact').on('hidden.bs.modal', function (e) {
            $('#modalExito').modal('show');
        });
    });

    $('#contact').on('show.bs.modal', function(e){
        console.log('El modal contacto se está mostrando');
    }); 
    $('#contact').on('shown.bs.modal', function(e){
        console.log('El modal contacto se mostró');
    });
    $('#contact').on('hide.bs.modal', function(e){
        console.log('El modal contacto se oculta');
    });
    $('#contact').on('hidden.bs.modal', function(e){
        console.log('El modal contacto se ocultó');
    });

    $('#btnCerrarExito').click(function () {
        $('#modalExito').modal('hide');
    });
});